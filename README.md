Srivastava N, Maneykowski P, Sowers RB. Algorithmic geolocation of harvest in hand-picked agriculture. Natural Resource Modeling. 2018;31:e12158. <https://doi.org/10.1111/nrm.12158>

By
Richard Sowers <r-sowers@illinois.edu>, website
<http://publish.illinois.edu/r-sowers/>

Visualization is at <https://www.youtube.com/watch?v=J71jKkatnVc>

algorithm.ipynb is main file.  It uses data.csv
interevals.py is a dictionary;  
* key:  phone IMEI
* value:  list of algorithmically identifies harvest intervals



