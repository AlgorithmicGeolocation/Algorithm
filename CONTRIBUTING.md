Peter Maneykowski (ISE undergraduate) did some early versions of the algorithm.
Nitin Srivastava (M.S. in ISE) wrote a thesis connected to this work.

Thanks to Maureen McGuire of Crisalida Farms.